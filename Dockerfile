# ubuntu-systemd

ARG DOCKER_REGISTRY_URL=registry.gitlab.com/docking/
ARG CUSTOM_VERSION=1.0
ARG UBUNTU_VERSION=18.04


FROM ${DOCKER_REGISTRY_URL}ubuntu-systemd-mini:${CUSTOM_VERSION}-${UBUNTU_VERSION} AS ubuntu-systemd

WORKDIR /root

RUN \
	apt-get -q -y update ; \
	apt-get -q -y install \
		cron logrotate rsyslog \
	; \
	apt-get -q -y clean ; \
	find /var/lib/apt/lists/ -type f -delete

RUN \
	sed -i \
		-e 's|^.*\(module(load="imklog"\)|#\1|' \
		/etc/rsyslog.conf

STOPSIGNAL SIGRTMIN+3

VOLUME /run /run/lock /tmp /sys/fs/cgroup

CMD exec /sbin/init
